<?php
	// requires php5
	define('UPLOAD_DIR', '../img/upload/');
	$img = $_POST['imgBase64'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$uniqid = uniqid();
	
	$fileName = $uniqid . '.png';
	$file = UPLOAD_DIR . $uniqid . '.png';
	$success = file_put_contents($file, $data);
	print $success ? $fileName : 'Unable to save the file.';
	//print_r($_POST);
?>