$(function() {


	//SET BACKGROUND FULL HEIGHT
	var height = $(window).height();
	$(".backgroundBG").css("height",height);
	
	var firstPage = 1;
	$("#page_"+firstPage).show();

});
//RESIZE WINDOW 
$(window).resize(function() {
	var height = $(window).height();
	$(".backgroundBG").css("height",height);
});

// --- FUNCTION --- GO BACK PAGE
function goBack() {
    window.history.back()
}
// --- FUNCTION --- CALL PAGE
function callPage(pageSelect) {
	$(".pageSeparate").hide();
    $("#page_"+pageSelect).fadeIn();
    
    
}



//OPEN Panel Option Share
$(document).on( "click", "#socialCallPanel", function( e ) {
	$(".socialOption").slideDown();
    $("#socialCallPanel").addClass("panelOpen");
});
$(document).on( "click", ".panelOpen", function( e ) {
	closeOptionSocial();
});
function closeOptionSocial() {
	$(".socialOption").slideUp();
	$("#socialCallPanel").removeClass("panelOpen");
}



//Show Image Hide Video
$(document).on( "click", "#snapPic", function( e ) {
	$(".snap").hide();
	
	
	if(window.mobileAndTabletcheck()){
		
	}
	else{
	$(".snap3").show();
	setTimeout(function(){
		$(".snap").hide();
		$(".snap2").show();
		
		setTimeout(function(){
			$(".snap").hide();
			$(".snap1").show();
			
			setTimeout(function(){
				$("#snapPic").hide();
				$(".boxVideo").hide();
				$(".btnCancel").hide();
				$(".backStep").hide();
				$(".snap").hide();
					
				$(".optionCamera").fadeIn();
				$(".boxImage").fadeIn();
			},1000);
			
		},1000);
		
	},1000);
	}
});


//Refresh Camera
$(document).on( "click", "#refreshCamera", function( e ) {
	refreshCamera();
});

function refreshCamera() {
	
	
	
	if(window.mobileAndTabletcheck()){
		$(".boxVideo").hide();
		$("#btimg").click();
		
		$("#snapPic").hide();
				$(".boxVideo").hide();
				$(".btnCancel").hide();
				$(".backStep").hide();
				$(".snap").hide();
					
				$(".optionCamera").fadeIn();
				$(".boxImage").fadeIn();
				
		}
		else{
		
		$(".boxVideo").fadeIn();
	$(".btnCancel").fadeIn();
	$(".backStep").fadeIn();
	$("#snapPic").fadeIn();
	
	$(".optionCamera").hide();
	$(".boxImage").hide();
	$(".optionFrame").hide();
	$(".backCamera").hide();
	closeOptionSocial();
	
		}




}



//Confirm Use Image
$(document).on( "click", "#imageConfirm", function( e ) {
	$(".optionCamera").hide();
	$(".boxImage").hide();
	
	$(".optionFrame").fadeIn();
	$(".backCamera").fadeIn();
});

//SELECT FRAME
$(document).on( "click", ".frame", function( e ) {
	var frameSelect = $(this).attr("rel");
	$(".frame").removeClass("selected");
	$(".frame"+frameSelect).addClass("selected");
	$("#selectFrameValue").val(frameSelect);
});

$(document).on( "click", "#confirmCameraAll", function( e ) {
	$("#bgPopup").show();
	
	
	var canvas2 = document.getElementById("canvas2"),
    ctx = canvas2.getContext("2d");

	canvas2.width = 746;
	canvas2.height = 678;


	var postcardBG = $("#selectFrameValue").val();
	var postcardName = $(".promiseForm #promiseName").val();
	var postcardText = $(".promiseForm #promiseText").val();
	
	var background = new Image();	
	if(postcardBG==1){
		background.src = "img/postcardOne.png";	
	}
	else{
		background.src = "img/postcardTwo.png";	
	}
	
	var getWpostcardName = textWidth(postcardName);
	var getWpostcardText = $("#promiseText").width();
	
	console.log(getWpostcardText);


// Make sure the image is loaded first otherwise nothing will draw.
background.onload = function(){
    ctx.drawImage(background,0,0);   
    


    //canvas.width = 200;
    
    ctx.drawImage(canvasHide,80,247); 
	
	var font = '22px arial';
	ctx.fillStyle = '#562c02';
	var getWpostcardNameP = ((getWpostcardName/2)+30);
	var textX = ((canvas2.width/2)-getWpostcardNameP);
	
	var getWpostcardTextP = ((getWpostcardText/2)-290)
	var textX2= ((canvas2.width/2)-getWpostcardTextP);
	
	ctx.font = font;
	ctx.fillText(postcardName, textX, 565);
	
	var maxWidth = 180;
    var lineHeight = 20;
      
      ctx.font = '16px arial';   
	 wrapText(ctx, postcardText, textX2, 280, maxWidth, lineHeight);
	//ctx.fillText(postcardText, textX2, 280);


	
	
	/////////////////////////  save รูป //////////////
	//var canvas = document.getElementById("canvas");
	 canvas2 = document.getElementById("canvas2"),
	 dataURL = canvas2.toDataURL();
	
	$.ajax({
		type: "POST",
		url: "system/tranferImage.php",
		data: { 
			imgBase64: dataURL
		}
	}).done(function(o) {
		console.log(o);
		console.log('saved');
		//$("#bgPopup").fadeOut();
		setTimeout(function(){
			refreshCamera();
			
			$(".postcardBox .postcardBG img").hide();
			$(".postcardBox .postcardBG img.postcard"+postcardBG).show();
			//Value
			$(".postcardName p").text(postcardName);
			$(".postcardText p").text(postcardText);
			callPage(4)
			$("#promiseImageValue").val(o);
			$("#showPic").html('<img  style=" width: 100%;" src="img/upload/'+o+'" />');
			
			var promiseName = $("#promiseName").val();
			var promiseText = $("#promiseText").val();
			var promiseEmail = $("#promiseEmail").val();
			var promiseImage = o;
			$.ajax({
				type: "GET",
				url: "system/save_db.php?promiseName="+promiseName+"&promiseText="+promiseText+"&promiseEmail="+promiseEmail+"&promiseImage="+promiseImage,
				data: { 
					imgBase64: promiseName
				}
				}).done(function(o) {
				if(o=="success"){
				console.log(o);
				console.log('saved2');
				
				}
				else{
					//alert(0);
				}
				$("#bgPopup").fadeOut();
			});
	
	
			
		},500);
	});
	}
	 /////////////////////////  end save รูป //////////////
});

function chunk(str, n) {
    var ret = [];
    var i;
    var len;

    for(i = 0, len = str.length; i < len; i += n) {
       ret.push(str.substr(i, n))
    }

    return ret
};



function wrapText(context, text, x, y, maxWidth, lineHeight) {



text = chunk(text, 25).join('/n');
        var cars = text.split("/n");
//var cars = text.split(" ");
        for (var ii = 0; ii < cars.length; ii++) {

            var line = "";
            var words = cars[ii].split(" ");

            for (var n = 0; n < words.length; n++) {
                var testLine = line + words[n] + " ";
                var metrics = context.measureText(testLine);
                var testWidth = metrics.width;
console.log(testWidth);
console.log(maxWidth);
                if (testWidth > maxWidth) {
                    context.fillText(line, x, y);
                    line = words[n] + " ";
                    y += lineHeight;
                }
                else {
                    line = testLine;
                }
            }

            context.fillText(line, x, y);
            y += lineHeight;
        }
     }
     

function textWidth(html_org){

  var html_calc = '<span>' + html_org + '</span>';
  $("#divText").html(html_calc);
  var width = $("#divText").find('span:first').width();
  return width;
};

//RESTART PROGRAM
function restartAgain() {
	$(".promiseForm #promiseName").val("");
	$(".promiseForm #promiseEmail").val("");
	$(".promiseForm #promiseText").val("");
	$("#selectFrameValue").val(1);
	refreshCamera();
	callPage(1);
	closeOptionSocial();
}



//Check Value Empty
$(document).on( "click", ".promiseForm .btnSendPromise", function( e ) {
	var name = $("#promiseName").val();
	var email = $("#promiseEmail").val();
	var text = $("#promiseText").val();
	if(name&&email&&text!=""){
		callPage(3);
		
		if(window.mobileAndTabletcheck()){
		$(".boxVideo").hide();
		$("#btimg").click();
		
		
		
		$("#snapPic").hide();
				$(".boxVideo").hide();
				$(".btnCancel").hide();
				$(".backStep").hide();
				$(".snap").hide();
					
				$(".optionCamera").fadeIn();
				$(".boxImage").fadeIn();
				
		}
		else{
		$(".boxVideo").show();	
		}
		return true;
		
	}else{
		$("#bgAlert").fadeIn();
		return false;
	}

});


$(document).on( "click", ".promiseForm .btnSendPromise", function( e ) {
	var name = $("#promiseName").val();
	var email = $("#promiseEmail").val();
	var text = $("#promiseText").val();
	if(name&&email&&text!=""){
		callPage(3);
		return true;
		
	}else{
		$("#bgAlert").fadeIn();
		return false;
	}

});

$(document).ready(function(){
    $("#btnSuccess").click(function(){
        $("#bgAlert").fadeOut();
    });
     $("#btnSuccessShared").click(function(){
        $("#bgPopupShared").fadeOut();
    });
    $("#email_share").click(function(){
        $("#bgPopupShared").fadeIn();
    });
     
});


$(document).on( "click", "#email_share", function( e ) {
	var promiseEmail = $("#promiseEmail").val();
			var promiseImage = $("#promiseImageValue").val();
			
			$.ajax({
				type: "GET",
				url: "system/share_email.php?promiseEmail="+promiseEmail+"&promiseImage="+promiseImage,
				/*
data: { 
					imgBase64: promiseName
				}
*/
				}).done(function(o) {
				if(o=="success"){
				console.log(o);
				console.log('saved2');
				
				}
				else{
					alert(o);
				}
				$("#bgPopup").fadeOut();
			});
});

		



$(document).on( "click", ".sharedSuccess", function( e ) {
$("#bgPopupShared").hide();
restartAgain();
});
	

