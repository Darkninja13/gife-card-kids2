// Put event listeners into place
window.addEventListener("DOMContentLoaded", function() {
	// Grab elements, create settings, etc.
		var canvas = document.getElementById("canvas"),
		    canvasHide = document.getElementById("canvasHide"),
			context = canvas.getContext("2d"),
			contextHide = canvasHide.getContext("2d"),
			video = document.getElementById("video"),
			imgmobile = document.getElementById("btimg"),
			videoObj = { "video": true },
			errBack = function(error) {
				console.log("Video capture error: ", error.code); 
			};

	// Put video listeners into place
		if(navigator.getUserMedia) { // Standard
			navigator.getUserMedia(videoObj, function(stream) {
				video.srcObject = stream;
				video.play();
			}, errBack);
		} else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
			navigator.webkitGetUserMedia(videoObj, function(stream){
				video.src = window.URL.createObjectURL(stream);
				video.play();
			}, errBack);
		} else if(navigator.mozGetUserMedia) { // WebKit-prefixed
			navigator.mozGetUserMedia(videoObj, function(stream){
				video.src = window.URL.createObjectURL(stream);
				video.play();
			}, errBack);
		}
	
	// Trigger photo take
	
	document.getElementById("snapPic").addEventListener("click", function() {
		setTimeout(function(){
			setTimeout(function(){
				setTimeout(function(){
					context.drawImage(video, 0, 0, 1080, 810);
					contextHide.drawImage(video, 0, 0, 337, 253);
				},1000);
			},1000);
		},1000);

	});
	
	
	

	
	document.getElementById('btimg').addEventListener('change', function(event) {
   // var myCanvas = document.getElementById('mycanvas');
   // var ctx = myCanvas.getContext('2d');
    var img = new Image();
    img.onload = function(){
        //myCanvas.width = img.width;
        //myCanvas.height = img.height;
        
        
        context.drawImage(img, 0, 0, 1080, 810);
        contextHide.drawImage(img, 0, 0, 337, 253);
        //console.log(canvas.toDataURL('image/jpeg'));
    };
    
    img.src = URL.createObjectURL(event.target.files[0]);
});




	
	
}, false);

