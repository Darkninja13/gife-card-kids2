<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<title>มาเขียนอวยพรน้องไตรกัน</title>
<script>
	
	window.mobileAndTabletcheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};



</script>
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/camera.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>
</head>
<body>
	<div id="container">
		<img class="backgroundBG" src="img/bg.jpg" />
		<div id="bgPopup" class="text-center bgDarkTran">
			<i class="fa fa-circle-o-notch fa-spin fa-3x"></i>
			<div class="clearfix"></div>
			<label>กรุณารอสักครู่</label>
		</div>
			<div id="bgAlert" class="text-center bgDarkTran">
				<div class="alertBox">
					<i class="fa fa-exclamation-triangle"></i>
					<div class="clearfix"></div>
					<label>กรุณากรอกข้อมูลให้ครบด้วยครับ</label>
					<div class="clearfix"></div>
					<button type="button" id="btnSuccess" class="btn btn-success btnAlertSuccess">ตกลง</button>
				</div>	
			</div>
			
			<div id="bgPopupShared" class="text-center bgDarkTran">
				<div class="alertBox">
					<img class="sharedSuccess" src="img/sharedSuccess.png" />
					<div class="clearfix"></div>
					<a href="javascript:void(0);" onclick="restartAgain()">
						<button type="button" id="btnSuccessShared" class="btn btn-success btnAlertSuccess">ตกลง</button>
					</a>
				</div>	
			</div>
		
		<div id="page_1" class="landingPage pageSeparate">
			<div class="content text-center">
				<img class="logoMain" src="img/logoMain.png">
				<div class="clearfix"></div>
				<img class="textThai" src="img/font-thai.png">
				<div class="textInfo">
					<strong>" ด.ช. ตรัย สุวิชัย "</strong>
					<p>
						วันที่ 28 สิงหาคม 2561<br>
						พระเยซูตรัสว่า “จงยอมให้เด็กเล็กๆ เข้ามาหาเรา อย่าห้ามเขาเลย <br>
						เพราะว่าชาวแผ่นดินสวรรค์เป็นของคนเช่นเด็กเหล่านั้น”<br>
						(มธ.19:14)<br>
						
					</p>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="btnNext text-center">
				<a href="javascript:void(0)" onclick="callPage(2)">
					<button type="button" class="btn btn-info btn-lg btnExtraLg">
						ร่วมแสดงความยินดี  <i class="fa fa-angle-right fa-fw"></i>
					</button>
				</a>
			</div>
		</div><!-- landingPage -->
		
		
		<div id="page_2" class="goodnessPage pageSeparate">
			<div class="topContent">
				<img class="logoMain" src="img/logoMain.png" />
				<div class="clearfix"></div>
				<img class="textThai" src="img/font-thai.png">
			</div>
			<div class="clearfix"></div>
			<div class="promiseBox">
				<img class="promiseBg" src="img/promiseFormBG.png" />
				<div class="promisePanel">
					<div class="promiseForm">
							<input type="text" id="promiseName"  class="promiseName" autocomplete="off"/>
							<div class="clearfix"></div>
							<input type="text" id="promiseEmail" class="promiseEmail" autocomplete="off"/>
							<div class="clearfix"></div>
							<textarea id="promiseText" class="promiseText"></textarea>
							<div class="clearfix"></div>
							<button type="submit" class="btn btn-primary btn-lg btnSendPromise btnNormaly">
								แสดงความยินดี <i class="fa fa-send fa-fw"></i>
							</button>
					</div>
					
					<div class="btnOption">
						<div class="col-xs-6 text-left">
							<button type="button" class="backStep">
								<a href="javascript:void(0);" onclick="callPage(1)">
									<div class="btn btn-warning btn-lg btnNormaly">
										<i class="fa fa-fw fa-angle-double-left fa-1x"></i> กลับ
									</div>
								</a>
							</button>
						</div>
						<div class="col-xs-6"></div>
						<div class="clearfix"></div>
					</div><!-- btnOption -->
					
					<div class="clearfix"></div>
				</div><!-- promisePanel -->
			</div><!-- promiseBox -->
		</div><!-- goodnessPage -->
		
		
		<div id="page_3" class="capturePage pageSeparate">
			<div class="topContent">
				<img class="logoMain" src="img/logoMain.png" />
				<div class="clearfix"></div>
				<img class="textThai" src="img/font-thai.png">
			</div>
			<div class="boxVideo text-center">
				<video id="video" width="1080" height="auto" autoplay></video>
				<div id="countSnap">
					<div class="snap snap3">3</div>
					<div class="snap snap2">2</div>
					<div class="snap snap1">1</div>
				</div>
			</div>
			<input style="display:none" id="btimg" type="file" accept="image/*" capture="camera">


			<div class="boxImage text-center">
			
				<canvas id="canvas" width="1080" height="810"></canvas>
				<canvas style="display:none" id="canvasHide" width="360" height="270"></canvas>
			</div>
			<div class="snapCap text-center">
				<div id="snapPic" class="btn btn-default btnExtraLg btnCircleLg">
					<i class="fa fa-camera fa-2x fa-fw"></i>
				</div>
				<div class="optionCamera">
					<div class="btnRefresh pull-left">
						<div id="refreshCamera" class="btn btn-primary btn-lg">
							<i class="fa fa-refresh fa-2x fa-fw"></i>
						</div>
					</div>
					<div class="btnConfirm pull-right">
						<a href="javascript:void(0);" id="imageConfirm">
							<div class="btn btn-success btn-lg">
								<i class="fa fa-check fa-2x fa-fw"></i>
							</div>
						</a>
					</div>
					<div class="clearfix"></div>
				</div><!-- optionCamera -->
				
				<div class="optionFrame">
					<div class="selectFrame">
						<div class="frame1 frame selected" rel="1">
							<img src="img/postcardOne.png" />
						</div>
						<div class="frame2 frame" rel="2">
							<img src="img/postcardTwo.png" />
						</div>
						<div style="  font-size: 22px; position: fixed; top: -999px;">
							<canvas id="canvas2" width="1080" height="810"></canvas>
						<div style="  font-size: 22px;" id="divText" ></div>
						</div>
					</div>
					<div class="btnOption">
						<div class="col-xs-6 text-left">
							<button type="button" class="backCamera">
								<a href="javascript:void(0);" onclick="refreshCamera()">
									<div class="btn btn-warning btn-lg btnNormaly">
										<i class="fa fa-fw fa-angle-double-left fa-1x"></i> กลับ
									</div>
								</a>
							</button>
						</div>
						<div class="col-xs-6">
							<div class="btnConfirm text-right" id="confirmCameraAll">
								<a href="javascript:void(0);">
									<div class="btn btn-primary btn-lg btnNormaly">
										เลือกใช้กรอบภาพนี้
									</div>
								</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div><!-- btnOption -->
				</div><!-- optionCamera -->
			</div><!-- snapCap -->
			<div class="clearfix"></div>
			<div class="btnOption">
				<div class="col-xs-6 text-left">
					<button type="button" class="backStep">
						<a href="javascript:void(0);" onclick="callPage(2)">
							<div class="btn btn-warning btn-lg btnNormaly">
								<i class="fa fa-fw fa-angle-double-left fa-1x"></i> กลับ
							</div>
						</a>
					</button>
				</div>
				<div class="col-xs-6"></div>
				<div class="clearfix"></div>
			</div><!-- btnOption -->
		</div><!-- capturePage -->
		
		
		<div id="page_4" class="postcardPage pageSeparate">
			<div class="topContent">
				<img class="logoMain" src="img/logoMain.png" />
				<div class="clearfix"></div>
				<img class="textThai" src="img/font-thai.png">
			</div>
			<div class="clearfix"></div>
			<div class="postcardBox">
				<div id="showPic" class="postcardBG2">
					<img  class="postcard1" src="img/postcardOne.png" />
					<!-- <img class="postcard2" src="img/postcardTwo.png" /> -->
				</div>
				<!--
<div class="postcardPanel">
					<img class="postcardImg" src="img/share.jpg" />
					<div class="postcardText">
						<p></p>
					</div>
					<div class="postcardName">
						<p></p>
					</div>
				</div>
--><!-- postcardPanel -->
			</div><!-- postcardBox -->
			
			<div class="clearfix"></div>
			<div class="btnOption">
				<div class="col-xs-4 text-center">
					<button type="button" class="backStep">
						<a href="javascript:void(0);" onclick="callPage(3)">
							<div class="btn btn-warning btn-lg btnNormaly">
								<i class="fa fa-fw fa-angle-double-left fa-1x"></i> กลับ
							</div>
						</a>
					</button>
				</div>
				<div class="col-xs-4 text-center">
					<a href="javascript:void(0);" onclick="restartAgain()">
						<div class="btn btn-success btn-lg btnNormaly">
							เริ่มต้นใหม่ <i class="fa fa-fw fa-refresh fa-1x"></i>
						</div>
					</a>
				</div>
				<div class="col-xs-4 text-center shareBox">
					<a href="javascript:void(0);" id="email_share">
						<div class="btn btn-primary btn-lg btnNormaly">
							<i class="fa fa-fw fa-save fa-1x"></i>
						</div>
					</a>
					<div class="socialOption">
						<div  class="btn btn-primary">
							<a id="email_share" href="javascript:void(0);">
								<i class="fa fa-envelope fa-fw"></i>
							</a>
						</div>
						
					</div>
				</div><!-- shareBox -->
			</div><!-- btnOption -->
			<div class="clearfix"></div>
			
		</div><!-- postcardPage -->
		
		
		
		<div class="hidden valuePage">
			<input type="hidden" id="selectFrameValue" value="1"/>
			<input type="hidden" id="promiseNameValue" />
			<input type="hidden" id="promiseEmailValue" />
			<input type="hidden" id="promiseTextValue" />
			<input type="hidden" id="promiseImageValue" />
		</div> 
	</div><!-- #container -->
</body>
</html>

